package com.danmamaliga.learnspringsecurity.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.security.RolesAllowed;

@RestController
public class TodoController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private static final List<Object> TODOS = List.of(new Todo("danmamaliga", "Learn Spring Security"),
			new Todo("danmamaliga", "Learn AWS"));

	@GetMapping("/todos")
	public List<Object> retrieveAllTodos() {
		return TODOS;
	}

	@GetMapping("/users/{username}/todos")
	@PreAuthorize("hasRole('USER') and #username == authentication.name")
	@PostAuthorize("returnObject.username == 'danmamaliga'")
	@RolesAllowed({"ADMIN", "USER"})
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public Object retrieveTodosForUser(@PathVariable String username) {
		return TODOS.get(0);
	}

	@PostMapping("/users/{username}/todos")
	public void createTodoForUser(@PathVariable String username, @RequestBody Todo todo) {
		logger.info("Create {} for {}", todo, username);
	}
}

record Todo(String username, String description) {
}